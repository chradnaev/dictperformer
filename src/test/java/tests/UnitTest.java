package tests;

import main.DictPerformer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.Duration;
import java.time.LocalTime;

public class UnitTest {
    private Logger logger = LogManager.getLogger(UnitTest.class);
    LocalTime beforeTime;
    @Before
    public void prepare() {
        beforeTime = LocalTime.now();
    }
    @After
    public void after() {
        System.out.println(Duration.between(beforeTime, LocalTime.now()).toMillis()+" ms elapced");
    }

    @Test
    public void wordSearchTest() throws InterruptedException, IOException {
        String[] filenames = new String[] {"test_words1.txt", "test_words2.txt", "test_words3.txt"};
        String out = "test_dict.txt";

        writeLines(filenames[0], "бета", "гамма");
        writeLines(filenames[1], "альфа", "омега");
        writeLines(filenames[2], "эпсилон", "икс");

        DictPerformer performer = new DictPerformer(filenames);
        performer.process(out);

        try {
            assertFileContains(out, "альфа", "бета", "гамма", "икс", "омега", "эпсилон");
        }
        finally {
            for (String filename : filenames) {
                Files.delete(Paths.get(filename));
            }
            Files.delete(Paths.get(out));
        }
    }

    private void writeLines(String filename, String ...lines) throws IOException {
        try (BufferedWriter writer = Files.newBufferedWriter(Paths.get(filename), StandardCharsets.UTF_8)) {
            for (String s : lines) {
                writer.write(s+" ");
            }
        }
    }
    private void assertFileContains(String filename, String ...lines) throws IOException {
        try (BufferedReader reader = Files.newBufferedReader(Paths.get(filename), StandardCharsets.UTF_8) ) {

            for (String s : lines) {
                String line = reader.readLine();
                System.out.println(line);
                Assert.assertEquals(s, line);
            }
        }
    }
}
