package main;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class WordParser implements Runnable {
    private static final Pattern wrdPattern = Pattern.compile("[А-Яа-яЁё]{2,}", Pattern.UNICODE_CASE);
    private Logger logger = LogManager.getLogger(WordParser.class);
    private final Set<String> results;
    private final ConsumerManager manager;

    public WordParser(Set<String> results, ConsumerManager manager) {

        this.results = results;
        this.manager = manager;
    }

    @Override
    public void run()  {
        Set<String> result = new HashSet<>();
        while (true) {
            String line;

            try {
                line = manager.consume();
                if (line == null) {
                    break;
                }
            } catch (InterruptedException e) {
                logger.error(e.getMessage());
                break;
            }

            Matcher matcher = wrdPattern.matcher(line);
            while (matcher.find()) {
                result.add(matcher.group().toLowerCase());
            }

        }
        synchronized (results) {
            results.addAll(result);
        }
    }
}
