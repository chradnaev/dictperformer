package main;


public interface ConsumerManager {
    boolean isAliveProducers();
    String consume() throws InterruptedException;
}
