package main;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.*;


public class DictPerformer {
    private Set<String> dictionary = new TreeSet<>();
    private String[] filenames;
    private final int parseThreadCount = 5;
    private final Logger logger = LogManager.getLogger(DictPerformer.class);


    public DictPerformer(String[] filenames) {
        this.filenames = filenames;
    }

    public void process(String out) throws InterruptedException, IOException {
        ExecutorService readService = Executors.newFixedThreadPool(filenames.length);

        ProducerConsumerManager manager = new ProducerConsumerManager();
        logger.trace("Started reading");
        for (String filename : filenames) {
            readService.execute(new LineReader(filename, manager));
        }

        ExecutorService parseService = Executors.newFixedThreadPool(parseThreadCount);
        for (int i=0; i < parseThreadCount; i++) {
            parseService.execute(new WordParser(dictionary, manager));
        }

        readService.shutdown();
        try {
            readService.awaitTermination(10, TimeUnit.SECONDS);
            readService.shutdownNow();
        } catch (Exception exc) {
            logger.error(exc.getMessage());
        }
        logger.trace("Ended reading");

        parseService.shutdown();
        try {
            parseService.awaitTermination(10, TimeUnit.SECONDS);
            parseService.shutdownNow();
        } catch (Exception exc) {
            logger.error(exc.getMessage());
        }
        logger.trace("Ended parsing");
        sortAndWrite(out);
    }
    public void sortAndWrite(String filename) throws IOException {
        Files.write(Paths.get(filename), dictionary);
    }

    public Set<String> getDictionary() {
        return dictionary;
    }


}
