package main;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.io.BufferedReader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;


public class LineReader implements Runnable {
    private Logger logger = LogManager.getLogger(LineReader.class);
    private String filename;
    private final ProducerManager manager;

    public LineReader(String filename, ProducerManager manager) {
        this.filename = filename;
        this.manager = manager;
        manager.incrementProducerCount();
    }

    @Override
    public void run()  {
        try (BufferedReader reader = Files.newBufferedReader(Paths.get(filename),StandardCharsets.UTF_8)) {
            logger.trace("reading '" + filename + "'");

            String line = reader.readLine();
            while (line != null) {
                //logger.trace("reading line '" + line.substring(0, Math.min(10, line.length())) + "...'");
                manager.produce(line);
                logger.trace("sleeping..");
                //Thread.sleep(50);
                line = reader.readLine();
            }
        } catch (Exception exc) {
            logger.error("reading '" + filename + "': " + exc.getMessage());
        }
        manager.decrementProducerCount();
        System.out.println("put end");
    }
}
