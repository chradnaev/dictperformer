package main;

public interface ProducerManager {
    void produce(String line);
    void incrementProducerCount();
    void decrementProducerCount();
}
