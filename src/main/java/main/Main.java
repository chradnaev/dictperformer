package main;

import java.io.IOException;

public class Main {
    public static void main(String[] args) throws InterruptedException, IOException {
        DictPerformer performer = new DictPerformer(args);
        performer.process("dict.txt");
    }
}
