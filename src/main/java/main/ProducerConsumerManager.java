package main;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

public class ProducerConsumerManager implements ProducerManager, ConsumerManager {
    private volatile int producerCount = 0;
    private final BlockingQueue<String> lineQueue = new LinkedBlockingQueue<>();
    private final Logger logger = LogManager.getLogger(ProducerConsumerManager.class);

    public ProducerConsumerManager() {
    }

    @Override
    public void produce(String line) {
        lineQueue.offer(line);
    }

    @Override
    public String consume() throws InterruptedException {
        if (isAliveProducers()) {
            String line = null;
            while (line == null && isAliveProducers()) {
                logger.trace("poll");
                line = lineQueue.poll(20, TimeUnit.MILLISECONDS);
            }
            return line;
        } else {
            return lineQueue.poll();
        }
    }

    @Override
    public boolean isAliveProducers() {
        return this.producerCount != 0;
    }

    @Override
    public synchronized void decrementProducerCount() {
        this.producerCount--;
    }

    @Override
    public synchronized void incrementProducerCount() {
        this.producerCount++;
    }
}
